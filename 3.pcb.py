import mysql.connector
import schedule
import time
import datetime
import sys

sys.path.append("data/")
sys.path.append("function/")
import detailAlarm
import currentAlarm
import db
import managePCB
import functionGeneral
import manageDevice


#ฟังก์ชันสำหรับตรวจสอบ IP ของ PCB ว่าอยู่ในสถานะอะไร
def autoCheckIP() :
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[14]

    global getPCBAll
    try:
        # ดึงข้อมูล บอร์ด pcb จากตาราง pcb ออกมาทั้งหมด
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getPCBAll')
        for result in cursor.stored_results():
            getPCBAll = result.fetchall()
    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    for result in getPCBAll:
         data = functionGeneral.functionCheckip(ip=result[1],round=5)
         #PacketLoss คือ การตัดสตริงที่ได้จากการปิงโดยเอาเฉพาะค่า Loss ออกมา เพื่อใช้ในการหาค่าความเสถียร์
         PacketLoss = float([x for x in data.decode('utf-8').split('\n') if x.find('Lost') != -1][0].split('%')[0].split(' ')[-1].split('(')[1])
         if(PacketLoss <= 2.5): #สัญญาณปกติ
             idcurrentstable = "PCBUNSTABLE-"+result[0] #สร้างขึ้นมาเพื่อที่จะเป็น id ของอลาม current
             idcurrentfail   =  "PCBFAIL-"+result[0]  #สร้างขึ้นมาเพื่อที่จะเป็น id ของอลาม current
             managePCB.updateStatusPCB(idpcb=result[0],statuspcb="Stable")
             currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentstable)
             currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentfail)
         elif(PacketLoss >2.5 and PacketLoss <= 10): #สัญญาณไม่เสถียร
             idcurrent = "PCBUNSTABLE-"+result[0]  #สร้างขึ้นมาเพื่อที่จะเป็น id ของอลาม current
             managePCB.updateStatusPCB(idpcb=result[0], statuspcb="Unstable")
             detail = detailAlarm.alarmDetail(alarm=11,pcbname=result[2],idpcb=result[0],devicename=None,iddevice=None,programerror=None)
             currentAlarm.insertCurrent(idcurrentview=idcurrent,settingalarm=5,detailalarm=detail)
             currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
         else: #สัญญาณผิดปกติ
             idcurrent = "PCBFAIL-"+result[0]   #สร้างขึ้นมาเพื่อที่จะเป็น id ของอลาม current
             managePCB.updateStatusPCB(idpcb=result[0], statuspcb="Fail")
             detail = detailAlarm.alarmDetail(alarm=10, pcbname=result[2], idpcb=result[0], devicename=None, iddevice=None,programerror=None)
             currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=4, detailalarm=detail)
             currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
             manageDevice.updateCurrentdevices(idpcb=result[0])

#เอาไว้ตรวจจับว่าอุปกรณ์วัดไฟที่อยู่ใน pcb เเต่ล่ะตัวทำงานเป็นไงโดยแบ่งออกเป็น
#  1.วัดค่าไม่ได้ทุกตัว ใน pcb ..
#  2.ตอนนี้อุปกรณ์ใน pcb .. วัดค่าได้สถานะต่ำกว่าเกณฑ์
#  3.ตอนนี้อุปกรณ์ใน pcb .. วัดค่าได้สถานะสูงกว่าเกณฑ์
def analyzepcb():
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[30]

    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getPCBAll')
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    if(get):
            # รหัสสำหรับตรวจจับความผิดปกติ
            idcurrent = db.errorprogram[31]

            for data in get:
                idpcb = data[0]  # คือ รหัสอุปกรณ์ PCB
                namepcb = data[2] # คือ ชื่อ PCB

                #analyzepcb คือ ตรวจสอบว่า pcb ตัวที่ค้นหา Fail หรือไม่
                analyzepcbFail = managePCB.getPCBcheckDeviceFailallFromIdpcb(idpcb=idpcb)
                # analyzepcb คือ ตรวจสอบว่า pcb ตัวที่ค้นหา Low หรือไม่
                analyzepcbLow = managePCB.getPCBcheckDeviceLowFromIdpcb(idpcb=idpcb)
                # analyzepcb คือ ตรวจสอบว่า pcb ตัวที่ค้นหา High หรือไม่
                analyzepcbHigh = managePCB.getPCBcheckDeviceHighFromIdpcb(idpcb=idpcb)

                # รหัสสำหรับตรวจจับความผิดปกติ -------------------
                idcurrentFail = "CURRENT-FAIL-PCB-" +str(idpcb)
                idcurrentLow = "CURRENT-LOW-PCB-" +str(idpcb)
                idcurrentHigh = "CURRENT-HIGH-PCB-" +str(idpcb)

                if(analyzepcbFail):

                    idcurrentFail = "CURRENT-FAIL-PCB-"+str(idpcb)
                    detail = detailAlarm.alarmDetail(alarm=4, pcbname=namepcb, idpcb=idpcb, devicename=None, iddevice=None,programerror=None)
                    currentAlarm.insertCurrent(idcurrentview=idcurrentFail, settingalarm=1, detailalarm=detail)
                    currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrentFail)

                    # อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ Fail
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentLow)
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentHigh)

                elif(analyzepcbLow):

                    idcurrentLow = "CURRENT-LOW-PCB-"+str(idpcb)
                    detail = detailAlarm.alarmDetail(alarm=5, pcbname=namepcb, idpcb=idpcb, devicename=None,iddevice=None, programerror=None)
                    currentAlarm.insertCurrent(idcurrentview=idcurrentLow , settingalarm=2, detailalarm=detail)
                    currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrentLow)

                    # อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ Low
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentFail)
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentHigh)

                elif(analyzepcbHigh):

                    idcurrentHigh = "CURRENT-HIGH-PCB-" + str(idpcb)
                    detail = detailAlarm.alarmDetail(alarm=6, pcbname=namepcb, idpcb=idpcb, devicename=None,iddevice=None, programerror=None)
                    currentAlarm.insertCurrent(idcurrentview=idcurrentHigh, settingalarm=3, detailalarm=detail)
                    currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrentHigh)

                    # อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ High
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentFail)
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentLow)

                else:

                    #ทำการอัพเดทสถานะอลามทุกตัวไม่ว่าจะเป็น Fail  ,Low ,High ให้เป็นสถานะว่า clear
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentFail)
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentLow)
                    currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentHigh)
    #ต้องดึงอุปกร


#ฟังก์ชันนี้ถือว่าสำคัญ เป็นฟังก์ชันที่เกิดเมื่ออุปกรณ์วัดกระเเสไฟทุกตัวมีปัญหา
#  1.อุปกรณ์วัดทุกตัวที่มีสถานะเปิด  วัดค่าไม่ได้เลย
#  2.อุปกรณ์วัดทุกตัววัดค่าได้สถานะต่ำกว่าเกณฑ์
#  3.อุปกรณ์วัดทุกตัววัดค่าได้สถานะสูงกว่าเกณฑ์
def analyzeMain():

    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceAllStatusFail')
        for result in cursor.stored_results():
            deviceAllFail = result.fetchall()

    except ConnectionError  as error:
        pass
    finally:
        cursor.close()
        cnx.close()


    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceAllStatusLow')
        for result in cursor.stored_results():
            deviceAllLow = result.fetchall()

    except ConnectionError  as error:
        pass
    finally:
        cursor.close()
        cnx.close()


    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceAllStatusHigh')
        for result in cursor.stored_results():
            deviceAllLHigh = result.fetchall()

    except ConnectionError  as error:
        pass
    finally:
        cursor.close()
        cnx.close()


    iddeviceAllFail = "CURRENT-FAIL-MAIN-FAIL"
    iddeviceAllLow  = "CURRENT-FAIL-MAIN-LOW"
    iddeviceAllLHigh = "CURRENT-FAIL-MAIN-HIGH"

    if(not deviceAllFail):
        detail = detailAlarm.alarmDetail(alarm=1, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=None)
        currentAlarm.insertCurrent(idcurrentview=iddeviceAllFail, settingalarm=1, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=iddeviceAllFail)

        #อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ Fail
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllLow)
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllLHigh)

    elif(not  deviceAllLow):
        detail = detailAlarm.alarmDetail(alarm=2, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=None)
        currentAlarm.insertCurrent(idcurrentview=iddeviceAllLow, settingalarm=1, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=iddeviceAllLow)

        # อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ Low
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllFail)
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllLHigh)

    elif(not deviceAllLHigh):
        detail = detailAlarm.alarmDetail(alarm=3, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=None)
        currentAlarm.insertCurrent(idcurrentview=iddeviceAllLHigh, settingalarm=1, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=iddeviceAllLow)

        # อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ High
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllFail)
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllLow)

    else:
        # ทำการอัพเดทสถานะอลามทุกตัวไม่ว่าจะเป็น Fail  ,Low ,High ให้เป็นสถานะว่า clear
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllFail)
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllLow)
        currentAlarm.updateStatusCurrentClear(idcurrentview=iddeviceAllLHigh)


# schedule.every(1).seconds.do(autoCheckIP)
# schedule.every(1).seconds.do(analyzepcb)
# schedule.every(1).seconds.do(analyzeMain)


schedule.every(1).minutes.do(analyzepcb)
schedule.every(1).minutes.do(analyzeMain)
schedule.every(5).minutes.do(autoCheckIP)


print("Programe pcb V.1.3")
print("online : ",datetime.datetime.now())
print("--------------------------------")
print("working.....")
while True:
    schedule.run_pending()
    time.sleep(1)
