import sys
from flask  import  Flask,request
from flask_restful import  Resource,Api
from flask_cors import CORS
import re  #regular-expression
import socket

sys.path.append("data/")
sys.path.append("function/")
import  analyDevice,currentAlarm
import detailAlarm,functionGeneral,insertData,manageDevice,managePCB
import db


app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})


class sendata(Resource):
    def post(selfself):
        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[1]

        some_json = request.get_json()
        getData = {'data': some_json}

        try:
                if(some_json != None):

                    if (re.findall("^36+\.0[\d][A-Z]+\.#[\d]", getData["data"]["No"])):

                        try:

                                #เป็นฟังก์ชันสำหรับจัดการอุปกรณ์PCB
                                    managePCB.managePCB(idpcb=getData["data"]["No"],ipaddress=getData["data"]["IPdevice"])
                                #เป็นฟังก์ชันสำหรับจัดการอุปกรณ์วัดกระเเสไฟ
                                    manageDevice.manageDevice(iddevice=getData["data"]["No"],idpcb=getData["data"]["No"],current=getData["data"]["Value"])
                                #เป็นฟังก์ชันสำหรับจัดการรายละเอียดอุปกรณ์วัดกระเเสไฟ
                                    manageDevice.manageDetail(iddevice=getData["data"]["No"])
                                # #ฟังก์ชันสำหรับตรวจสอบว่าอุปกรณ์ที่รับเข้ามานั้นมีสถานะหรือ ระดับการวัดอยู่ที่เท่าไหร่
                                    analyDevice.getDevicesfromID(iddevice=getData["data"]["No"])
                        finally:
                                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)


        except Exception as error:


            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)


        return "success"

api.add_resource(sendata, '/sendata')

if __name__ == "__main__":
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[2]
    try:
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)
            app.run(host=socket.gethostbyname(socket.gethostname()), debug=True, port=5000, threaded=True)

    except  Exception as error:

            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)

