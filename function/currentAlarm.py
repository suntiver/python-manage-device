import mysql.connector
import  sys
sys.path.append("../data/")

import db

#เป็นฟังก์ชันสำหรับเพิ่มเหตุการณ์อลามที่เกิดขึ้น
#   1.เพิ่มอลามที่เกิดขึ้นลงในตาราง currentviews
def  insertCurrent(idcurrentview,settingalarm,detailalarm):

    try:
        #เพิ่มอลามลงในตาราง currenview
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idcurrentview,settingalarm,detailalarm]
        cursor.callproc('insertCurrent', data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)
    finally:
        cursor.close()
        cnx.close()


#เป็นฟังก์ชันอัพเดทสถานะของอลามว่า unClear
def updateStatusCurrentUnClear(idcurrentview):

    try:
        #อัพเดทสถานะอลามตาม idcurrentview
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idcurrentview]
        cursor.callproc('updateStatusCurrentUnClear', data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)

    finally:
        cursor.close()
        cnx.close()



#เป็นฟังก์ชันอัพเดทสถานะของอลามว่า Clear
def updateStatusCurrentClear(idcurrentview):

    try:
        #อัพเดทสถานะอลามตาม idcurrentview
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idcurrentview]
        cursor.callproc('updateStatusCurrentClear', data)
        cnx.commit()
    except ConnectionError  as error:
        print("can't insert alarm to db  beacuse: ",error)
    finally:
        cursor.close()
        cnx.close()



#เป็นฟังก์ชัน สำหรับ ดึง current ออกมา ที่มีสาถนะ clear เเล้ว 3 วันนับจากวันปัจจุบัน
#   1.ดึง current ออกมา ที่มีสถานะ clear เเล้ว 3 วัน ago
#   2.ทำการเช็คว่ามีข้อมูลหรือไม่
#   3.ทำการเพิ่มข้อมูลลงตาราง historycurrentview
#   4.ทำการลบ ข้อมูลในตาราง currentview โดย ลบจาก idcurrentview
def  manageCurrent_3Ago():

    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getCurrentviewsAll')
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
          print(error)

    finally:
        cursor.close()
        cnx.close()

    if(get):
        for result in get:
                insertHistorycurrentView(currentViewsid=result[0], settingAlarmid=result[1], detailData=result[2], statusData=result[3],datetime=result[4])
                deleteCurrentviewFromID(idcurrentview=result[0])


#ฟังก์ชันนี้สำหรับเพิ่มลงในตาราง currentView
def insertHistorycurrentView(currentViewsid,settingAlarmid,detailData,statusData,datetime):

    try:
        #เพิ่มอลามลงในตาราง historycurrentview
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [currentViewsid,settingAlarmid,detailData,statusData,datetime]
        cursor.callproc('insertHistorycurrentView', data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)
    finally:
        cursor.close()
        cnx.close()


# ฟังก์ชันนี้สำหรับลบ อลามในตาราง currentview ตาม id
def deleteCurrentviewFromID(idcurrentview):
    try:
        # ลบข้อมูลในตาราง currentview
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idcurrentview]
        cursor.callproc('deleteCurrentviewFromID',data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)
    finally:
        cursor.close()
        cnx.close()


