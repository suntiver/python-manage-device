import mysql.connector
import  json
import sys
sys.path.append("data/")
sys.path.append("function/")

import db
import functionGeneral


# ดึงข้อมุลเฉพาะ 4H ที่นานเกิน 3 เดือน
# เป็นฟังก์ชัน สำหรับ สำรองข้อมูล 4H ลงใน ไฟล์ text
def backup_data():
    try:

        payload = []
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getData4H_less_3Months')
        for result in cursor.stored_results():
            get = result.fetchall()

        for result in get:
                content = {"id_4Hours":result[0],"devices_id":result[1],"avg_current":result[2],
                           "current_low":result[3],"current_high":result[4],"datetime":result[5].strftime("%Y-%m-%d %H:%M:%S"),
                           "createdAt":result[6].strftime("%Y-%m-%d %H:%M:%S"),"updatedAt":result[7].strftime("%Y-%m-%d %H:%M:%S")
                           }
                payload.append(content)


        #บันทึกข้อมูลลงไฟล์ json
        if(payload != []):
            functionGeneral.whitefilejosn(my_details=payload)
            payload.clear()


    except ConnectionError  as error:
         print(error)
    finally:
        cursor.close()
        cnx.close()



# ลบข้อมูล ตาราง 1minutes  ที่นานเกิน 3 เดือน
def delete_data_1Minute():
    try:
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = ["1minute"]
        cursor.callproc('deleteData_lest_3Months',data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)

    finally:
        cursor.close()
        cnx.close()



# ลบข้อมูล ตาราง 15minutes  ที่นานเกิน 3 เดือน
def delete_data_15Minutes():
    try:
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = ["15minutes"]
        cursor.callproc('deleteData_lest_3Months',data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)

    finally:
        cursor.close()
        cnx.close()


# ลบข้อมูล ตาราง 30minutes  ที่นานเกิน 3 เดือน
def delete_data_30Minutes():
    try:
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = ["30minutes"]
        cursor.callproc('deleteData_lest_3Months',data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)

    finally:
        cursor.close()
        cnx.close()



# ลบข้อมูล ตาราง 1hour  ที่นานเกิน 3 เดือน
def delete_data_1H():
    try:
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = ["1hour"]
        cursor.callproc('deleteData_lest_3Months',data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)

    finally:
        cursor.close()
        cnx.close()



# ลบข้อมูล ตาราง 4hours  ที่นานเกิน 3 เดือน
def delete_data_4H():
    try:
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = ["4hours"]
        cursor.callproc('deleteData_lest_3Months',data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)

    finally:
        cursor.close()
        cnx.close()



# ลบข้อมูล ตาราง 24hours  ที่นานเกิน 3 เดือน
def delete_data_24H():
    try:
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = ["24hours"]
        cursor.callproc('deleteData_lest_3Months',data)
        cnx.commit()
    except ConnectionError  as error:
        print(error)

    finally:
        cursor.close()
        cnx.close()
