#เป็นฟังก์ชันที่เกี่ยวกับรายละเอียดอลาม
def alarmDetail(alarm,pcbname,idpcb,devicename,iddevice,programerror):
    if(alarm == 1):
         return "Phase 3 - Mian - All pcb boards detected no current."
    elif(alarm == 2):
         return "Phase 3 - Main - All pcb boards are detecting current lower than referent value."
    elif (alarm == 3):
         return "Phase 3 - Main - All pcb boards are detecting current higher than referent value."
    elif (alarm == 4):
         return "Phase 3 - "+pcbname+"("+idpcb+")"+" - All clamps on a pcb board detected no current."
    elif (alarm == 5):
        return "Phase 3 - " + pcbname + "(" + idpcb + ")" + " - All clamps on a pcb board  are detecting current lower  than referent value."
    elif (alarm == 6):
        return "Phase 3 - " +pcbname+ "(" + idpcb + ")"+" - All clamps on a pcb board  are detecting current higher than referent value."
    elif (alarm == 7):
        return "Phase 3 - " +devicename+"("+iddevice+")"+" - A clamp detected no current."
    elif (alarm == 8):
        return "Phase 3 - " + devicename + "(" + iddevice + ")" + " - A clamp is detecting current lower than referent value."
    elif (alarm == 9):
        return "Phase 3 - " + devicename + "(" + iddevice + ")" + " - A clamp is detecting current higher than referent value"
    elif (alarm == 10):
        return "Phase 3 - " +pcbname+"("+idpcb+")"+" - A pcb can't reach the destination"
    elif (alarm == 11):
        return "Phase 3 - " +pcbname+"("+idpcb+")"+" - A pcb board reaching unstable"
    elif (alarm == 12):
        return "Phase 3 - " +str(programerror)
