import subprocess
import  json
import  time
import sys
sys.path.append("function/")
import manageData

#เป็นฟังก์ชัน PING หา IP โดยจะ return มาเป็น 0 กับ 1
#   1.รับข้อมูล IP เเละ จำนวนรอบมาเเล้วทำการตรวจสอบ
def functionCheckip(ip,round):
    info = subprocess.STARTUPINFO()
    info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    info.wShowWindow = subprocess.SW_HIDE

    output = subprocess.Popen(['ping', '-n',str(round), ip], stdout=subprocess.PIPE, startupinfo=info).communicate()[0]
    return output


def whitefilejosn(my_details):

    today = time.strftime("%Y%m%d")
    try:
        with open('backup data/' + today + '.json', 'w') as json_file:
            json.dump(my_details, json_file)

    except IOError:
            print(IOError)

    finally:
            # ลบข้อมูล 4H ที่มีเวลามากกว่า 3 เดือน
            manageData.delete_data_4H()
