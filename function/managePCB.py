import mysql.connector

import  sys
sys.path.append("../data/")
sys.path.append("../function/")

import db
import detailAlarm
import currentAlarm


#เป็นฟังก์ชันสำหรับจัดการอุปกรณ์PCB
#   1.ตรวจสอบอุปกรณ์ PCB ใน DB ว่าซ่ำกันหรือเปล่า
#   2.เพิ่มอุปกรณ์ PCB ลง ใน DB
#   3.อัพเดท IP บนอุปกรณ์ PCB
def managePCB(idpcb,ipaddress):

    # นำ idpcb มาทำการเเยกองค์ประกอบ โดยเอาเฉพาะที่ต้องการ
    str_round1 = idpcb.split('#')
    str_round2 = str_round1[0].split('.')
    idpcb = str_round2[0] + "." + str_round2[1]
    # ------------------------------------------

    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[26]
    global checkPCBinDB
    try:
        # การเช็คอุปกรณ์ PCB
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idpcb,"@variable"]
        cursor.callproc('getPCB',data)
        for result in cursor.stored_results():
            checkPCBinDB = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    if(checkPCBinDB[0][0] == 0):
        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[27]
        try:
            # การเพิ่มอุปกรณ์ใหม่ PCB
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()
            data = [idpcb,ipaddress]
            cursor.callproc('InsertPCB', data)
            cnx.commit()
        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)
    else:
        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[28]
        try:
            # การอัพเดทอุปกรณ์
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()
            data = [idpcb,ipaddress]
            cursor.callproc('upDatePCB', data)
            cnx.commit()
        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)




#เป็นฟังก์ชันสำหรับอัพเดทอุปกรณ์
#   1.อัพเดทอุปกรณ์ PCB
def  updateStatusPCB(idpcb,statuspcb):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[29]
    try:
        #อัพเดทสถานะอุปกรณ์ PCB
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idpcb,statuspcb]
        cursor.callproc('updateStatusPCB', data)
        cnx.commit()
    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)



# ฟังก์ชันสำหรับตรวจสอบว่าตัวเเคมป์ไฟตัวไหนบ้างที่ มีค่ามากกว่า 0.01 ขึ้นไป เพื่อใช้ประเมินว่า pcb ตัวนั้น Fail หรือไม่
#   1.ถ้าคิวรีเเล้วพบอุปกรณ์จะต้องส่ง False ออกไปเพื่อระบุว่า PCB ไม่ได้ Fail
def getPCBcheckDeviceFailallFromIdpcb(idpcb):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[31]

    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idpcb]
        cursor.callproc('getPCBcheckDeviceFailallFromIdpcb', data)
        for result in cursor.stored_results():
            getData = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)

    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)


    if(getData):
        return False
    else:
        return  True


# ฟังก์ชันสำหรับตรวจสอบว่าตัวเเคมป์ไฟตัวไหนบ้างที่ มีค่าต่ำ 0.01 เเละ มีค่า มากกว่าค่าต่ำที่กำหนด เพื่อใช้ประเมินว่า pcb ตัวนั้น Low หรือไม่
#   1.ถ้าคิวรีเเล้วพบอุปกรณ์จะต้องส่ง False ออกไปเพื่อระบุว่า PCB ไม่ได้ Low
def getPCBcheckDeviceLowFromIdpcb(idpcb):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[31]

    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idpcb]
        cursor.callproc('getPCBcheckDeviceLowFromIdpcb', data)
        for result in cursor.stored_results():
            getData = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)

    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)


    if(getData):
        return False
    else:
        return  True



# ฟังก์ชันสำหรับตรวจสอบว่าตัวเเคมป์ไฟตัวไหนบ้างที่ มีค่าต่ำกว่าค่าสูงที่กำหนด เพื่อใช้ประเมินว่า pcb ตัวนั้น High หรือไม่
#   1.ถ้าคิวรีเเล้วพบอุปกรณ์จะต้องส่ง False ออกไปเพื่อระบุว่า PCB ไม่ได้ High
def getPCBcheckDeviceHighFromIdpcb(idpcb):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[31]

    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idpcb]
        cursor.callproc('getPCBcheckDeviceHighFromIdpcb', data)
        for result in cursor.stored_results():
            getData = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)

    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)


    if(getData):
        return False
    else:
        return  True