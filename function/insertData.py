import mysql.connector
import  sys
sys.path.append("../data/")
sys.path.append("../function/")
import  db
import detailAlarm
import currentAlarm

#ฟังก์ชันนี้คือ เพิ่มข้อมูลลงในตาราง 1 นาที
def inserData_1minute(iddevice,currentvalue):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[16]
    try:
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        # รหัสอุปกรณ์ ,ค่ากระเเสไฟ
        data = [iddevice,currentvalue]
        cursor.callproc('insertData_1minutes', data)
        cnx.commit()
    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

#ฟังก์ชันนี้คือ เพิ่มข้อมูลลงในตาราง 15 นาที
def insertData_15minutes(deviceid,avgcurrent,currentlow,currenthigh):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[17]
    try:
        #เพิ่มอลามลงในตาราง เพิ่มข้อมูลลงในตาราง 15 นาที
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [deviceid,avgcurrent,currentlow,currenthigh]
        cursor.callproc('insertData_15minutes', data)
        cnx.commit()
    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

#ฟังก์ชันนี้คือ เพิ่มข้อมูลลงในตาราง 30 นาที
def insertData_30minutes(deviceid,avgcurrent,currentlow,currenthigh):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = "PROGRAMEERROR-16"
    try:
        #เพิ่มอลามลงในตาราง เพิ่มข้อมูลลงในตาราง 30 นาที
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [deviceid,avgcurrent,currentlow,currenthigh]
        cursor.callproc('insertData_30minutes', data)
        cnx.commit()
    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

#ฟังก์ชันนี้คือ เพิ่มข้อมูลลงในตาราง 60 นาที
def insertData_1Hour(deviceid,avgcurrent,currentlow,currenthigh):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[18]
    try:
        # เพิ่มอลามลงในตาราง เพิ่มข้อมูลลงในตาราง 60 นาที
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [deviceid, avgcurrent, currentlow, currenthigh]
        cursor.callproc('insertData_1Hour', data)
        cnx.commit()
    except ConnectionError  as error:

        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)

    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

#ฟังก์ชันนี้คือ เพิ่มข้อมูลลงในตาราง 240 นาที
def insertData_4Hours(deviceid,avgcurrent,currentlow,currenthigh):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[19]
    try:
        # เพิ่มอลามลงในตาราง เพิ่มข้อมูลลงในตาราง 240 นาที
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [deviceid, avgcurrent, currentlow, currenthigh]
        cursor.callproc('insertData_4Hours', data)
        cnx.commit()
    except ConnectionError  as error:

        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)

    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

#ฟังก์ชันนี้คือ เพิ่มข้อมูลลงในตาราง 1440 นาที
def insertData_24Hours(deviceid,avgcurrent,currentlow,currenthigh):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[20]
    try:
        # เพิ่มอลามลงในตาราง เพิ่มข้อมูลลงในตาราง 1440 นาที
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [deviceid, avgcurrent, currentlow, currenthigh]
        cursor.callproc('insertData_24Hours', data)
        cnx.commit()
    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)
