import mysql.connector
import  sys
sys.path.append("../data/")
sys.path.append("../function/")
import db
import currentAlarm
import detailAlarm




#ฟังก์ชันสำหรับตรวจสอบว่าอุปกรณ์ที่รับเข้ามานั้นมีสถานะหรือ ระดับการวัดอยู่ที่เท่าไหร่
#   1.ตรวจสอบว่า ตัวแคมป์ที่รับเข้ามามีสถานะ เปิดหรือเปล่า ถ้าเปิดก็ดึงข้อมูลออกมา
def getDevicesfromID(iddevice):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[15]
    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [iddevice]
        cursor.callproc('getDevicesfromID', data)
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    #ตรวจสอบว่าอุปกรณ์มีมาหรือเปล่า
    if(get):

          device_id = get[0][0]
          device_name = get[0][1]
          device_settingHigh = get[0][2]
          device_settingLow  = get[0][3]
          device_current   = get[0][4]

          # รหัสสำหรับตรวจจับความผิดปกติ
          idcurrentFail = "CURRENT-FAIL-CAMP-" + str(device_id)
          idcurrentLow = "CURRENT-LOW-CAMP-" + str(device_id)
          idcurrentHigh = "CURRENT-HIGH-CAMP-" + str(device_id)


          if(device_current < 0.01):
            # รหัสสำหรับตรวจจับความผิดปกติ

                detail = detailAlarm.alarmDetail(alarm=7, pcbname=None, idpcb=None, devicename=device_name, iddevice=device_id,programerror=None)
                currentAlarm.insertCurrent(idcurrentview=idcurrentFail,settingalarm=1, detailalarm=detail)
                currentAlarm.updateStatusCurrentUnClear(idcurrentview= idcurrentFail)

                # อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ Fail
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentLow)
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentHigh)

          elif(device_current >= 0.01 and device_current <= device_settingLow):
            # รหัสสำหรับตรวจจับความผิดปกติ

                detail = detailAlarm.alarmDetail(alarm=8, pcbname=None, idpcb=None, devicename=device_name, iddevice=device_id,programerror=None)
                currentAlarm.insertCurrent(idcurrentview=idcurrentLow, settingalarm=2, detailalarm=detail)
                currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrentLow)


                # อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ Low
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentFail)
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentHigh)

          elif (device_current >= device_settingHigh):
            # รหัสสำหรับตรวจจับความผิดปกติ

                detail = detailAlarm.alarmDetail(alarm=9, pcbname=None, idpcb=None, devicename=device_name, iddevice=device_id, programerror=None)
                currentAlarm.insertCurrent(idcurrentview=idcurrentHigh , settingalarm=3, detailalarm=detail)
                currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrentHigh )

                # อัพสถานะอลามที่ไม่เกี่ยวข้อง กับ High
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentFail)
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentLow)
          else:

             #อัพเดทสถานะอลามว่า clear ถ้าอุปกรณ์ตัวนั้นกลับมาปกติเเล้ว
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentFail)
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentLow)
                currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrentHigh)