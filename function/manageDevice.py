import mysql.connector
import  sys
sys.path.append("../data/")
sys.path.append("../function/")

import db
import detailAlarm
import currentAlarm
#เป็นฟังก์ชันสำหรับจัดการอุปกรณ์วัดกระเเสไฟ
#  1.ตรวจสอบอุปกรณ์ว่ามีซ่ำใน DB หรือเปล่า
#  2.เพิ่มอุปกรณ์ตัวใหม่ลงใน DB
#  3.อัพเดทอุปกรณ์ต่อจากเดิมลงใน DB
def  manageDevice(iddevice,idpcb,current):

    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[21]
    global checkdeviceindb
    try:
        #การเช็คอุปกรณ์
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data  = [iddevice,"@variable"]
        cursor.callproc('getDevice',data)

        for result in cursor.stored_results():
            checkdeviceindb = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    # นำ idpcb มาทำการเเยกองค์ประกอบ โดยเอาเฉพาะที่ต้องการ
    str_round1 = idpcb.split('#')
    str_round2 = str_round1[0].split('.')
    idpcb = str_round2[0] + "." + str_round2[1]

    if (checkdeviceindb[0][0] == 0):
        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[22]
        try:
            # การเพิ่มอุปกรณ์ใหม่
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()
            data = [iddevice,idpcb,current]
            cursor.callproc('InsertDevice', data)
            cnx.commit()
        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)
    else:
        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[23]
        try:
            # การอัพเดทอุปกรณ์
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()
            data = [iddevice,idpcb,current]
            cursor.callproc('upDateDeivce', data)
            cnx.commit()
        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)


#เป็นฟังก์ชันสำหรับจัดการรายละเอียดอุปกรณ์วัดกระเเสไฟ
#  1.เพิ่ม รายละเอียดอุปกรณ์ลงในฐานข้อมูล
def  manageDetail(iddevice):

    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[24]
    try:
        # การเพิ่มรายละเอียดอุปกรณ์
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [iddevice]
        cursor.callproc('insertDetailDevice', data)
        cnx.commit()
    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)


#เป็นฟังก์ชันอัพเดทสถานะอุปกรณ์วัดกระเเสไฟ โดยจะอัพเดทเมื่อตรวจพบความผิดปกติของอุปกรณ์ PCB
def updateCurrentdevices(idpcb):
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[25]
    try:
        # อัพเดทสถานะอุปกรณ์วัดกระเเสไฟ ให้เป็น 0.00
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        data = [idpcb]
        cursor.callproc('updateCurrentdevices', data)
        cnx.commit()
    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)