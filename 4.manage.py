import schedule
import time
import datetime
import sys

sys.path.append("data/")
sys.path.append("function/")

import currentAlarm
import db
import detailAlarm
import manageData



# เป็นฟังก์ชัน สำหรับ ดึง current ออกมา ที่มีสาถนะ clear เเล้ว 3 วันนับจากวันปัจจุบัน
def manageCurrent():
     # รหัสสำหรับตรวจจับความผิดปกติ
     idcurrent = db.errorprogram[32]
     try:

        currentAlarm.manageCurrent_3Ago()
     except Exception:
         detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(Exception))
         currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
         currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
     finally:
         currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)


# เป็นฟังก์ชัน สำหรับ ลบข้อมูล ตาม ID ถ้าข้อมูลน้อยกว่า 3 วันนับจากวันปัจจุบัน
def delete_data():

    def delete_1Minute():

        # ลบข้อมูล 1mitue ที่มีเวลามากกว่า 3 เดือน
        manageData.delete_data_1Minute()

    def delete_15minutes():

        # ลบข้อมูล 15mitues ที่มีเวลามากกว่า 3 เดือน
        manageData.delete_data_15Minutes()

    def delete_30minutes():

        # ลบข้อมูล 30mitues ที่มีเวลามากกว่า 3 เดือน
        manageData.delete_data_30Minutes()

    def delete_1Hour():

        # ลบข้อมูล 1H ที่มีเวลามากกว่า 3 เดือน
        manageData.delete_data_1H()

    def delete_4Hours():

        #บันทึกข้อมูลลงไฟล์ JSON
        manageData.backup_data()

    def delete_24Hours():
        # ลบข้อมูล 24H ที่มีเวลามากกว่า 3 เดือน
        manageData.delete_data_24H()

    #call function
    delete_1Minute()
    delete_15minutes()
    delete_30minutes()
    delete_1Hour()
    delete_4Hours()
    delete_24Hours()



schedule.every().day.at("23:00").do(delete_data)
schedule.every().day.at("23:59").do(manageCurrent)


print("Programe manage  V.1.3")
print("online : ",datetime.datetime.now())
print("--------------------------------")
print("working.....")

while True:
    schedule.run_pending()
    time.sleep(1)