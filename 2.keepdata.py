import mysql.connector
import schedule
import time
import datetime
import sys

sys.path.append("data/")
sys.path.append("function/")
import detailAlarm
import currentAlarm
import insertData
import db

def keep_data_every_1Minute():

    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[3]
    global get
    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceOpen')
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    for result in get:
        insertData.inserData_1minute(iddevice=result[0],currentvalue=result[1])

def keep_data_every_15Minutes():

    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[4]
    global get
    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceOpen')
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    for result in  get:
        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[5]
        try:
            # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()

            data = [result[0],15]  #result[0] = คือ รหัสอุปกรณ์วัด ,  15 คือ จำนวนตัวที่จะเอามาโดยอิงตามนาที
            cursor.callproc('getData_1Minutes',data)
            for result in cursor.stored_results():
                getData = result.fetchall()

        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

        templist = []
        for  result2 in getData:
             templist.append(result2[1])

        avg = sum(templist) / len(templist)
        maxValue = max(templist)
        minValue = min(templist)

        insertData.insertData_15minutes(deviceid=getData[0][0],avgcurrent=avg,currentlow=minValue,currenthigh=maxValue)
        templist.clear()

def keep_data_every_30Minutes():
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[6]
    global get
    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceOpen')
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    for result in get:
        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[7]
        try:
            # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()

            data = [result[0], 30]  # result[0] = คือ รหัสอุปกรณ์วัด ,  30 คือ จำนวนตัวที่จะเอามาโดยอิงตามนาที
            cursor.callproc('getData_1Minutes', data)
            for result in cursor.stored_results():
                getData = result.fetchall()

        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

        templist = []
        for result2 in getData:
            templist.append(result2[1])

        avg = sum(templist) / len(templist)
        maxValue = max(templist)
        minValue = min(templist)

        insertData.insertData_30minutes(deviceid=getData[0][0], avgcurrent=avg, currentlow=minValue, currenthigh=maxValue)
        templist.clear()

def keep_data_every_1Hour():
    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[8]

    global get
    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceOpen')
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    for result in get:
        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[9]
        try:
            # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()

            data = [result[0], 60]  # result[0] = คือ รหัสอุปกรณ์วัด ,  60 คือ จำนวนตัวที่จะเอามาโดยอิงตามนาที
            cursor.callproc('getData_1Minutes', data)
            for result in cursor.stored_results():
                getData = result.fetchall()

        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

        templist = []
        for result2 in getData:
            templist.append(result2[1])

        avg = sum(templist) / len(templist)
        maxValue = max(templist)
        minValue = min(templist)

        insertData.insertData_1Hour(deviceid=getData[0][0], avgcurrent=avg, currentlow=minValue, currenthigh=maxValue)
        templist.clear()

def keep_data_every_4Hours():

    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[10]
    global get
    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceOpen')
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None,programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    for result in get:

        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[11]
        try:
            # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()

            data = [result[0], 240]  # result[0] = คือ รหัสอุปกรณ์วัด ,  240 คือ จำนวนตัวที่จะเอามาโดยอิงตามนาที
            cursor.callproc('getData_1Minutes', data)
            for result in cursor.stored_results():
                getData = result.fetchall()

        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

        templist = []
        for result2 in getData:
            templist.append(result2[1])

        avg = sum(templist) / len(templist)
        maxValue = max(templist)
        minValue = min(templist)

        insertData.insertData_4Hours(deviceid=getData[0][0], avgcurrent=avg, currentlow=minValue, currenthigh=maxValue)
        templist.clear()

def keep_data_every_24Hours():

    # รหัสสำหรับตรวจจับความผิดปกติ
    idcurrent = db.errorprogram[12]
    global get
    try:
        # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
        cnx = mysql.connector.connect(**db.config)
        cursor = cnx.cursor()
        cursor.callproc('getDeviceOpen')
        for result in cursor.stored_results():
            get = result.fetchall()

    except ConnectionError  as error:
        detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
        currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
        currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
    finally:
        cursor.close()
        cnx.close()
        currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

    for result in get:

        # รหัสสำหรับตรวจจับความผิดปกติ
        idcurrent = db.errorprogram[13]
        try:
            # ตรวจสอบอุปกรณ์วัดที่มีสถานะ Open เท่านั้น
            cnx = mysql.connector.connect(**db.config)
            cursor = cnx.cursor()

            data = [result[0], 1440]  # result[0] = คือ รหัสอุปกรณ์วัด ,  1440 คือ จำนวนตัวที่จะเอามาโดยอิงตามนาที
            cursor.callproc('getData_1Minutes', data)
            for result in cursor.stored_results():
                getData = result.fetchall()

        except ConnectionError  as error:
            detail = detailAlarm.alarmDetail(alarm=12, pcbname=None, idpcb=None, devicename=None, iddevice=None, programerror=str(error))
            currentAlarm.insertCurrent(idcurrentview=idcurrent, settingalarm=6, detailalarm=detail)
            currentAlarm.updateStatusCurrentUnClear(idcurrentview=idcurrent)
        finally:
            cursor.close()
            cnx.close()
            currentAlarm.updateStatusCurrentClear(idcurrentview=idcurrent)

        templist = []
        for result2 in getData:
            templist.append(result2[1])

        avg = sum(templist) / len(templist)
        maxValue = max(templist)
        minValue = min(templist)

        insertData.insertData_24Hours(deviceid=getData[0][0], avgcurrent=avg, currentlow=minValue, currenthigh=maxValue)
        templist.clear()


schedule.every(1).minutes.do(keep_data_every_1Minute)
schedule.every(15).minutes.do(keep_data_every_15Minutes)
schedule.every(30).minutes.do(keep_data_every_30Minutes)
schedule.every().hour.do(keep_data_every_1Hour)
schedule.every(4).hours.do(keep_data_every_4Hours)
schedule.every().day.at("23:59").do( keep_data_every_24Hours)


print("Programe keepData  V.1.3")
print("online : ",datetime.datetime.now())
print("--------------------------------")
print("working.....")

while True:
    schedule.run_pending()
    time.sleep(1)
